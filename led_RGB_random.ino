/*
 *********************Arduino Source File Header**************************
  __file_name__ = "led_RGB_random.ino"
  __description__="Pilotare struiscia led RGB 12V con BC377 "
  __author__ = "Mauro Ricciardi"
  __copyright__ = "Informazioni di Copyright"
  __license__ = "GPL"
  __email__ = "mauro.ricciardi1965@gmail.com"
  __status__ = "Development[X]";"Test[]";"Production[]";
  __History__: ()
  __version__ = "1.0"
***************************************************************************
*/
/*
  ################################################
  ############ Collegamento INterfaccia LED ######
  ################################################
  #    INterfaccia   |   Arduino                 #
  #                                              #
  #        VCC 12V  >>>    NC                    #
  #        GND      >>>    GND                   #
*/
#define GREEN_PIN 5
#define RED_PIN 6
#define BLUE_PIN 3



void setup() {

  // init RED Led
  pinMode(GREEN_PIN, OUTPUT);
  //init GREEN Led
  pinMode(RED_PIN, OUTPUT);
  //led BLUE Led
  pinMode(BLUE_PIN, OUTPUT);
  //init seriale for debug
  Serial.begin(9600);

  //TEST LEDs
  digitalWrite (GREEN_PIN, HIGH);
  delay (100);
  digitalWrite (GREEN_PIN, LOW);
  delay (100);
  digitalWrite (RED_PIN, HIGH);
  delay (100);
  digitalWrite (RED_PIN, LOW);
  delay (100);
  digitalWrite (BLUE_PIN, HIGH);
  delay (100);
  digitalWrite (BLUE_PIN, LOW);
  delay (50);

}
void loop() {
  //int n = 0;
  int n = random (0, 100);
  for (int i = 0 ; i < n; i++)
  {
    //Fader RED led 0 to max
    int pwm_red =  random (0 , 255);
    int pwm_green =  random (0 , 255);
    int pwm_blue =  random (0 , 255);
    int delaytime = random (0, 500);
    int flash = random (0, 200);

    digitalWrite (GREEN_PIN, LOW);
    digitalWrite (RED_PIN, LOW);
    digitalWrite (BLUE_PIN, LOW);
    delay (delaytime);
    analogWrite (GREEN_PIN, pwm_red);
    analogWrite (RED_PIN, pwm_green);
    analogWrite (BLUE_PIN, pwm_blue);
    delay (flash);
    Serial.print ("Delay : ");
    Serial.println (delaytime);
    Serial.print ("flash time: ");
    Serial.println (flash);
    Serial.println (i);
  }

  Serial.println ("fine ciclo");
  delay (200);
  digitalWrite (GREEN_PIN, HIGH);
  delay (100);
  digitalWrite (GREEN_PIN, LOW);
  delay (100);
  digitalWrite (RED_PIN, HIGH);
  delay (100);
  digitalWrite (RED_PIN, LOW);
  delay (100);
  digitalWrite (BLUE_PIN, HIGH);
  delay (100);
  digitalWrite (BLUE_PIN, LOW);
  delay (50);
  

}
